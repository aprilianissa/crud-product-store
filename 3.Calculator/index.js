const kalkulator = (input) => {
    const split = input.split(" ")
    const operand1 = Number(split[0])
    const operand2 = Number(split[2])
    let result
    switch(split[1]){
        case "+" : 
        result = operand1 + operand2
        break
        case "-" : 
        result = operand1 - operand2
        break
        case "*" : 
        result = operand1 * operand2
        break
        case "/" : 
        result = operand1 / operand2
        break

    }
    return result
}

console.log(kalkulator("13 + 187"))
console.log(kalkulator("134 - 11"))
console.log(kalkulator("8 * 7"))
console.log(kalkulator("16 / 4"))