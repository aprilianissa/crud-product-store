// eslint-disable-next-line
import 'antd/dist/antd.min.css';
import './App.css';
import 'antd/dist/antd.less';
import TableComponent from './table';
// import { data } from './table/constantData';
import { useProduct } from './GlobalState';

function App() {
  const products = useProduct()
  return (
    <div className='App'>
      <h1>Marie Mart</h1>
      <TableComponent source={products.state} />
    </div>
  );
}

export default App;
