import React from 'react';
import { createContext, useContext, useReducer } from 'react';
const initialState = [
    {
      id: 1,
      name: 'Tas Selempang',
      address: '429 Yundt Center\nJaniyaport, AK 09433',
      origin: 'Greenland',
      weight: 400,
      stock: 30,
      price: 34199,
    },
    {
      id: 2,
      name: 'Sandisk 32GB',
      address: '87 Norinia East\nJayapura, Papua',
      origin: 'England',
      weight: 170,
      stock: 20,
      price: 80000,
    },
    {
      id: 3,
      name: 'Tripod Liger',
      address: '56 SouthAsnaria \n Harleafist, Heroland',
      origin: 'Fantagio',
      weight: 560,
      stock: 30,
      price: 29000,
    },
  ]
const ProductContext = createContext();

const productReducer = (state, action) => {
    const clone = [...state]
    const filtered = clone.filter((product) => product.id !== action.payload.id)
    switch (action.type) {
        case 'add': {
        return [...state, action.payload]
        }
        case 'edit': {
        const newProducts = [...filtered, action.payload]
        return newProducts.sort((a,b) => a.id - b.id)
        }
        case 'delete': {
        return filtered
        }
        default: {
        throw new Error(`Unhandled action type: ${action.type}`)
        }
    }
}

const ProductProvider = ({ children }) => {
  // useReducer
  const [state, dispatch] = useReducer(productReducer, initialState);  
  // Make variable `value` and assign state & dispatch
  const value = {state,dispatch}
  return <ProductContext.Provider value={value}>{children}</ProductContext.Provider>
}

const useProduct = () => {
  // fill the default value of useContext
  const context = useContext(ProductContext);

  if (context === 'undefined') {
    throw new Error('useProduct must be used within a ProductProvider')
  }

  return context;
}

export { ProductProvider, useProduct } 