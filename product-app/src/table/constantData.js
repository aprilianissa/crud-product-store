  let data = [
    {
      key: '1',
      id: 1,
      name: 'Tas Selempang',
      address: '429 Yundt Center\nJaniyaport, AK 09433',
      origin: 'Greenland',
      weight: 400,
      stock: 30,
      price: 34199,
    },
    {
      key: '2',
      id: 2,
      name: 'Sandisk 32GB',
      address: '87 Norinia East\nJayapura, Papua',
      origin: 'England',
      weight: 170,
      stock: 20,
      price: 80000,
    },
    {
      key: '3',
      id: 3,
      name: 'Tripod Liger',
      address: '56 SouthAsnaria \n Harleafist, Heroland',
      origin: 'Fantagio',
      weight: 560,
      stock: 30,
      price: 29000,
    },
  ];

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  /* eslint-disable no-template-curly-in-string */
  
  const validateMessages = {
    required: `This input is required!`,
    types: {
      name: `This input is not a valid name!`,
      address: `This input is not a valid number!`,
    },
  };

export {data, validateMessages, layout}