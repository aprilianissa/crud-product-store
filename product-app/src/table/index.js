import { Button, Modal, Row, Col, Table, Input } from 'antd';
import React, { useState } from 'react';
import {FaTrashAlt} from 'react-icons/fa'
import {BsFillPencilFill} from 'react-icons/bs'
import { useProduct } from '../GlobalState';

const TableComponent = ({source}) => {
  const products = useProduct()
  const columns = [
    {
      title: 'No',
      dataIndex: 'id',
      key: 'id',
      render: (text) => <p>{text}</p>,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text) => <p>{text}</p>,
      sorter: (a, b) => a.name[0].charCodeAt(0) - b.name[0].charCodeAt(0),
      
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Origin',
      dataIndex: 'origin',
      key: 'origin',
    },
    {
      title: 'Weight (gr)',
      dataIndex: 'weight',
      key: 'weight',
    },
    {
      title: 'Stock',
      dataIndex: 'stock',
      key: 'stock',
    },
    {
      title: 'Price (Rp)',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Action',
      key: 'action',
      render: (record) => (
        <Row gutter={[6,0]}>
          <Col>
            <Button onClick={()=>showEditModal(record)} shape='circle'><BsFillPencilFill color='blue' size={12}/></Button>
          </Col>
          <Col>
            <Button danger onClick={()=>{
              setProduct(record)
              onDeleteProduct(record)}} 
              shape='circle'>
                <FaTrashAlt color='red' size={12}/>
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const [isEditing, setIsEditing] = useState(false)
  const [isAdd, setIsAdd] = useState(false)
  const [product, setProduct] = useState({
    id: 1,
    name: 'Tas Selempang',
    address: '429 Yundt Center\nJaniyaport, AK 09433',
    origin: 'Greenland',
    weight: 400,
    stock: 30,
    price: 34199,
  })
  const showEditModal = (record) => {
    setIsEditing(true)
    setProduct(record)
  }

  const onDeleteProduct = (record) => {
    Modal.confirm({
      title: `Are you sure, you want to delete this products record? \n name: ${record.name}`,
      okType: 'danger' ,
      okText: 'Yes',
      onOk: ()=>{
        products.dispatch({type: 'delete', payload: record})
      }
    })
  }
  const onEditSubmit = () => {
    setIsEditing(false)
    products.dispatch({type:'edit', payload: product})
  }
  const handleCancel = () => {
    setIsEditing(false)
    setIsAdd(false)
  }
  const showAddModal = () => {
    setIsAdd(true)
    products.state = [] ? setProduct({...product, id: 1}) : setProduct({...product, id: ((source[source.length-1].id) +1)})
    
  }
  const onAddproduct = () => {
    products.state = [] ? setProduct({...product, id: 1}) : setProduct(product)
    products.dispatch({type: 'add', payload: product})
    setIsAdd(false)
  };
  return (
    <>
      <div className='add-product'>
        <Button onClick={showAddModal}>Add product</Button>
      </div>
      <Table columns={columns} dataSource={source} />
      <Modal title="Add products" visible={isAdd} onCancel={handleCancel} okText='Add' onOk={onAddproduct}>
      <>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Name</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.name}  onChange={(e)=> setProduct({...product, name : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Address</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.address}  onChange={(e)=> setProduct({...product, address : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Origin</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.origin}  onChange={(e)=> setProduct({...product, origin : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Weight</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.weight}  onChange={(e)=> setProduct({...product, weight : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Stock</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.stock}  onChange={(e)=> setProduct({...product, stock : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Price</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.price}  onChange={(e)=> setProduct({...product, price : e.target.value})}/>
                  </div>
                </Col>
            </div>
        </>
      </Modal>
      <Modal title="Edit products" visible={isEditing} okText='Save' onOk={onEditSubmit} onCancel={handleCancel}>
        <>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Name</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.name}  onChange={(e)=> setProduct({...product, name : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Address</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.address}  onChange={(e)=> setProduct({...product, address : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Origin</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.origin}  onChange={(e)=> setProduct({...product, origin : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Weight</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.weight}  onChange={(e)=> setProduct({...product, weight : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Stock</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.stock}  onChange={(e)=> setProduct({...product, stock : e.target.value})}/>
                  </div>
                </Col>
            </div>
            <div className='label-input'>
              <div className='label'>
                <Col>
                  <label>Price</label>
                </Col>
              </div>
                <Col span={20}>
                  <div className='input'>
                  <Input value={product?.price}  onChange={(e)=> setProduct({...product, price : e.target.value})}/>
                  </div>
                </Col>
            </div>
        </>
      </Modal>
    </>
  )
};

export default TableComponent;